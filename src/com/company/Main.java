package com.company;

import game.Human;
import game.Elf;
import game.Slime;
import game.Dragon;
import game.Robot;
import game.Mermaid;

public class Main {

    public static void main(String[] args) {
        Human human = new Human("JhonCena ");
        Elf elf = new Elf("Herrington ");
        Slime slime = new Slime("Rimuru ");
        Dragon dragon = new Dragon("Bahamut ");
        Robot robot = new Robot("Doraemon ");
        Mermaid mermaid = new Mermaid("Starbucks ");

        System.out.println(human);
        human.doAttack(" Din ");
        human.doMagic(" Win ");
        human.doMaxskill();
        System.out.println();

        System.out.println(elf);
        elf.doAttack(" Bow ");
        elf.doMagic(" Wood ");
        elf.doMaxskill();
        System.out.println();

        System.out.println(slime);
        slime.doAttack(" Tentacle ");
        slime.doMagic(" Cherubim ");
        slime.doMaxskill();
        System.out.println();

        System.out.println(dragon);
        dragon.doAttack(" Fire ");
        dragon.doMagic(" PapapaPaPaPapaPaPa ");
        dragon.doMaxskill();
        System.out.println();

        System.out.println(robot);
        robot.doAttack(" kuukihou ");
        robot.doMagic(" Yozigenketto ");
        robot.doMaxskill();
        System.out.println();

        System.out.println(mermaid);
        mermaid.doAttack(" Frappuccino ");
        mermaid.doMagic(" Sea ");
        mermaid.doMaxskill();
        System.out.println();

    }
}
