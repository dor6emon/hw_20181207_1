package game;

public class Human extends Roles {
    public Human(String name) {
        super(name);
    }

    @Override
    public void doAttack() {

        System.out.println("I'm " + getName() + " I use Wrestle ");
    }

    @Override
    public void doAttack(String weapon) {

        System.out.println("I'm " + getName() + " I use " + weapon + " Wrestle ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm " + getName() + " I use Da+DaDa-Da ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " Da+DaDa-Da ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm " + getName() + " I use " + " You Can't See Me ");
    }
}
