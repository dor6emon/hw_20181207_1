package game;

public class Mermaid extends Roles {
    public Mermaid(String name){
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("I'm " + getName() + " I use coffee ");
    }

    @Override
    public void doAttack(String weapon) {
        System.out.println("I'm " + getName() + " I use " + weapon + " coffee ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm " + getName() + " I use song ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " song ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm " + getName() + " I use tsunami ");
    }
}