package game;

public class Slime extends Roles {
    public Slime(String name){
        super(name);
    }

    @Override
    public void doAttack() {

        System.out.println("I'm " + getName() + " I use dissolve ");
    }

    @Override
    public void doAttack(String weapon) {

        System.out.println("I'm " + getName() + " I use " + weapon + " dissolve ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm " + getName() + " I use explosion ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " explosion ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm " + getName() + " I use consume ");
    }
}