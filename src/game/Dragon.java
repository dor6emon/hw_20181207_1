package game;

public class Dragon extends Roles {
    public Dragon(String name){
        super(name);
    }

    @Override
    public void doAttack() {

        System.out.println("I'm " + getName() + " I use ice ");
    }

    @Override
    public void doAttack(String weapon) {

        System.out.println("I'm " + getName() + " I use " + weapon + " ice ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm " + getName() + " I use yee ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " yee ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm " + getName() + " I use Purple ");
    }
}