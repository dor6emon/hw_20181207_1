package game;

public class Robot extends Roles {
    public Robot(String name){
        super(name);
    }

    @Override
    public void doAttack() {

        System.out.println("I'm " + getName() + " I use takekoputa- ");
    }

    @Override
    public void doAttack(String weapon) {

        System.out.println("I'm " + getName() + " I use " + weapon + " takekoputa- ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm " + getName() + " I use dokodemodoa ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " dokodemodoa ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm " + getName() + " I use taimumashin ");
    }
}