package game;

/**
 * Interface for common behaviors
 */
public interface IActions {
    public void doAttack();
    public void doAttack(String weapon);
    public void doMagic();
    public void doMagic(String magic);
    public void doMaxskill();
}
