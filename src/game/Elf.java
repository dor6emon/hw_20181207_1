package game;

public class Elf extends Roles {
    public Elf(String name){
        super(name);
    }

    @Override
    public void doAttack() {

        System.out.println("I'm " + getName() + " I use shoot ");
    }

    @Override
    public void doAttack(String weapon) {

        System.out.println("I'm " + getName() + " I use " + weapon + " shoot ");
    }

    @Override
    public void doMagic() {
        System.out.println("I'm "+getName()+" I use nature ");
    }

    @Override
    public void doMagic(String magic) {
        System.out.println("I'm " + getName() + " I use " + magic + " nature ");
    }

    @Override
    public void doMaxskill() {
        System.out.println("I'm "+getName()+" I use Shots Shots Shots .... ");
    }
}
